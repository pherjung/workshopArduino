# Workshop arduino avec ArduBlock
Ce dépôt contient plusieurs exercices. Chaque exercice contient une image affichant le schéma électrique et un fichier abp donnant la solution.
Concernant ArduBlock, j'ai utilisé la version `ardublock-all-20130712.jar`. À priori, les fichiers devraient être compatibles avec la dernière version d'Ardublock.

## Voici la liste des exercices:
 * Allumer une DEL avec la broche mis à LOW
 * Allumer une DEL avec la broche mis à HIGH

 * Allumer 3 DELs en même temps
 * Allumer 3 DELs à la suite. Débuter par la DEL de gauche
 * Allumer 3 DELs à la suite. Débuter par la DEL de droite

 * Allumer la DEL en restant appuyé sur le bouton
 * Utiliser le bouton comme interrupteur (comme à la maison)
 * Utiliser le bouton pour allumer 3 DELs et les faires clignoter ensemble

 * Faire tourner un servo 90 degrés d'un côté, 90 degrés de l'autre
 * Faire tourner un servo 90 degrés d'un côté, 90 degrés de l'autre après avoir appuyé sur le bouton
 * Faire tourner un servo 90 degrés d'un côté, 90 degrés de l'autre après avoir appuyé sur le bouton. Appuyer une deuxième fois sur le bouton interrompt le servo

 * Ajouter le capteur ultrason et dès qu'on s'approche avec la main du capteur le feu passe au rouge.
 * Créer un feu rouge "méchant" qui se met au rouge lorsqu'on approche la main à moins de ~ 10 cm et le met au vert à plus de 10 cm /!\ Incomplet /!\
